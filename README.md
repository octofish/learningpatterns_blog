Learning Patterns
=================

[![Netlify Status](https://api.netlify.com/api/v1/badges/8c6ac5d9-d97b-4450-ba39-9213099c16a2/deploy-status)](https://app.netlify.com/sites/learningpatterns/deploys)

Source for a site/blog by Will Furnass about research software engineering, systems administration and teaching.

See [https://learningpatterns.me][learningpatterns] for the rendered site.

Content published under a [Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa-4] license.

The site/blog is generated from Markdown (`.md`) files 
using the [Cryogen][cryogen] static site generator and
Cryogen itself is written in the [Clojure][clojure] functional programming language.

The site/blog is built by and hosted on [Netlify][netlify-prj]
using a custom domain.

Building and locally serving the site
-------------------------------------

1. Ensure that [Leiningen][lein] (Clojure project management and build tool) installed.
1. Clone this repository:
   ```sh
   mkdir ~/dev
   git clone https://gitlab.com/willfurnass/learningpatterns_blog
   cd learningpatterns_blog
   ```
1. Build and serve the site locally
   ```sh
   lein ring server
   ```
1. Alternatively just build the site:
   ```sh
   lein run
   ```

Site testing and deployment
---------------------------

When you create a pull request against this repository or 
push to a branch in this repository 
Netlify automatically checks that site can be built 
then deploys the site if on the `master` branch.


[cc-by-sa-4]: https://creativecommons.org/licenses/by-sa/4.0/
[clojure]: https://clojure.org/
[cryogen]: http://cryogenweb.org/
[learningpatterns]: https://learningpatterns.me
[lein]: https://leiningen.org/
[netlify-prj]: https://app.netlify.com/sites/learningpatterns/ 
