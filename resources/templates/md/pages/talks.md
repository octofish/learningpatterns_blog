{:title "Talks"
 :layout :page
 :page-index 1
 :navbar? true}

Here are talks I've given over the last couple of years on research software-related topics:

<table>
  <tr>
    <th>what?</th>
    <th>for?</th>
    <th>when?</th>
    <th>slides</th>
    <th>repo</th>
  </tr>
  <tr>
    <td>Intro to Conda</td>
    <td>RSE Sheffield seminar series</td>
    <td>2019-02-27</td>
    <td><a href="https://learningpatterns.me/conda-rses-pres">slides</a></td>
    <td><a href="https://github.com/willfurnass/conda-rses-pres">repo</a></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>


| HPC facilities inc. departmental resources                                                          | UoS Computer Science         | 2019-02-12 | [slides](https://rse.shef.ac.uk/rse-dcs-pres-on-hpc/)             | [repo](https://github.com/RSE-Sheffield/rse-dcs-pres-on-hpc)
| Make it right, then make it go fast enough (with [Phil Tooley](https://twitter.com/acceleratedsci)) | UoS Physics Theory Club      | 2019-01-04 | [slides](https://rse.shef.ac.uk/20190104-Theory_Club/)            | [repo](https://github.com/RSE-Sheffield/20190104-Theory_Club)    |
| Intro to version control                                                                            | Insigneo Institute           | 2018-07-05 | [slides](https://learningpatterns.me/vers-ctrl-pres-insig-2018/)  | [repo](https://github.com/willfurnass/vers-ctrl-pres-insig-2018) |
| Update for [Research Retreat](https://avouros.github.io/MLRetreat2017/MLRetreat2017.html)           | UoS Machine Learning group   | 2017-06-02 | [slides](https://learningpatterns.me/dcs-ml-away-day-jun-2017/)   | [repo](https://github.com/willfurnass/dcs-away-day-apr-2017)     |
| Introduction to Conda                                                                               | Python Sheffield User Group  | 2017-05-29 | [slides](https://willfurnass.github.io/conda-pyshef-pres)         | [repo](https://github.com/willfurnass/conda-pyshef-pres)         |
| Intro to the Jupyter Notebook, Numpy and Pandas                                                     | Python Sheffield User Group  | 2015-02-24 | [slides](https://willfurnass.github.io/pyshef-jupyter-numpy-pres) | [repo](https://github.com/willfurnass/pyshef-jupyter-numpy-pres) |
