{:title "About"
 :layout :page
 :page-index 0
 :navbar? true}

Will Furnass is [Research Software Engineer](http://rse.ac.uk/).

The path to this point has not been particularly direct: 
he has a computer science degree, 
has worked as a IT systems engineer in the film industry, 
has a PhD plus post-doc experience in water engineering 
(where he developed semi-physical and data-driven models of water quality in water distribution networks) 
and has provided support to the users of the University of Sheffield's high-performance computing clusters. 

In addition he has taught or helped run RSE, water engineering and study skills workshops. 
His interests include helping researchers optimise data analysis workflows (primarily using higher-level languages), 
providing training in RSE best practices and 
systems administration.

Twitter: [@willfurnass](https://www.twitter.com/willfurnass)
