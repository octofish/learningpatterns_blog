{:title "Helper for generating TLS CSRs and certs"
 :layout :post
 :tags  ["tls", "csr", "x.509", "python", "script", "lazy"]
 :toc true
 :draft? true
 }

I always forget how to generate TLS certificate signing requests (CSRs) and self-signed certificates
that are tied to multiple fully-qualified domain names ([Subject Alternative Name][san] certificates), 
so I wrote a wee script to generate them.  To generate a CSR and self-signed cert for

* `web1.learningpatterns.me`
* `web1.learningpatterns.com`
* `web2.learningpatterns.me`
* `web2.learningpatterns.com`

then print a human-readable representation of the CSR you run the following script like so:

```sh
./uos_openssl_gen.py \
    --hostnames web1,web2 \
    --domains learningpatterns.me,learningpatterns.com \
    --csr \
    --selfcert \
    --print-csr
```

Here's the script (GitHub Gist):

<script src="https://gist.github.com/willfurnass/86e0662631433c8f9e8a9b523df0747d.js"></script>
[san]: https://en.wikipedia.org/wiki/Subject_Alternative_Name
